# flux a!edition

## What is flux?

Flux is a WIP gamemode framework designed with performance and convenience in mind. It comes with "batteries included" and features all you need to create engaging experiences with maximum comfort. Whether you are a developer wishing to create something with Flux, or a person looking to create their own server, Flux makes it easy to achieve your goals, and gives you confidence of knowing everything will run smoothly.

## a!edition

Flux was stopped developing by their owners on 12/3/2019. 
sipq will continue developing and maintaining flux on new branch.

## Alpha release
Current version of Flux is currently in active development as an open alpha. This means that you can install it and it will run, but there will almost inevitably be bugs and issues, as well as a lot of missing features. If you are not a developer, it is probably better for you to wait until Flux is in beta.
